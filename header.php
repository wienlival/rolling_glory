<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Your Company</title>
    <!-- SEO Meta Tags-->
    <meta name="description" content="Yourcompany - E-commerce">
    <meta name="keywords" content="bootstrap, shop, e-commerce, market, modern, responsive,  business, mobile, bootstrap, html5, css3, js, gallery, slider, touch, creative, clean">
    <meta name="author" content="Wienli">
    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="css/theme.css">
    <link rel="stylesheet" media="screen" href="css/custom.css">
</head>
<body class="handheld-toolbar-enabled">
<div class="navbar-sticky bg-light">
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <a href="index.php">
                <img src="img/logo-dummy@2x.png" width="200" alt="Your Company">
            </a>
        </div>
    </div>
</div>
