<?php include 'header.php';?>
<div class="container">
    <div class="px-4 py-3 mb-5">
        <div class="px-lg-3">
            <div class="row">
                <div class="col-lg-7 pe-lg-0 pt-lg-4">
                    <div class="div-tag" style="width: 550px">
                        <img class="image-zoom" alt="Product image">
                    </div>
                </div>
                <div class="col-lg-5 pt-4 pt-lg-0">
                    <div class="pb-3">
                        <h2 class="title"></h2>
                        <div class="star-rating"></div>
                        <span class="d-inline-block fs-sm align-middle mt-1 ms-1 text-body review">
                        </span>
                    </div>
                    <div class="mb-3">
                        <span class="text-success"><i class="fa fa-star-of-david"></i></span>
                        <span class="text-success poin"></span>
                        <span class="stock" style="font-weight: bold"></span>
                    </div>
                    <div class="fs-sm mb-4 little-desc"></div>
                    <div class="form-group--number mb-3">
                        <button class="up" type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="fa fa-plus"></i></button>
                        <button class="down" type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="fa fa-minus"></i></button>
                        <input name="quantity" class="form-control" min="1" value="1" type="number" readonly="">
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <button class="btn btn-primary d-block w-10" type="button" data-bs-toggle="tooltip" title="Add to wishlist"><i class="fa fa-heart"></i></button>
                        </div>
                        <div class="col-md-5">
                            <button class="btn btn-success d-block w-100" type="submit">Redeem</button>
                        </div>
                        <div class="col-md-5">
                            <button class="btn btn-outline-success d-block w-100" type="submit">Add to Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row align-items-center py-md-3">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active">Info Produk</a>
                </li>
            </ul>
            <div class="col-lg-12">
                <h2 class="h3 mb-4 pb-2">Rincian</h2>
                <div class="desc"></div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include 'footer.php';?>
<script>
    var id = <?php echo $_GET['id']; ?>;

    var settings = {
        "url": "https://recruitment.dev.rollingglory.com/api/v2/gifts/"+id,
        "method": "GET",
        "timeout": 0,
    };
    var list = document.getElementById('product_list');

    $.ajax(settings).done(function (response) {
        $(".image-zoom").attr("src",response.data.attributes.images);
        $(".desc").append(response.data.attributes.description);
        $(".title").html(response.data.attributes.name);
        $(".review").html(response.data.attributes.numOfReviews + " reviews");
        $(".poin").html(response.data.attributes.points + " poins");
        $(".little-desc").html(response.data.attributes.info);
        if(response.data.attributes.stock<5){
            $(".stock").html(" Stock < 5");
            $(".stock").attr("class","text-danger");
        }else if(response.data.attributes.stock=0){
            $(".stock").html(" Sold Out");
            $(".stock").attr("class","text-danger");
        }else{
            $(".stock").html(" In Stock");
            $(".stock").attr("class","text-success");
        }

        if(Math.round(response.data.attributes.rating)>=4 && response.data.attributes.numOfReviews>25 && response.data.attributes.isNew==1){
            var label = document.createElement('img');
            label.setAttribute('class', 'tag-label-detail');
            label.src = "img/Group 3036.svg";
            $(".div-tag").append(label);
        }else if(Math.round(response.data.attributes.rating)>=4 && response.data.attributes.numOfReviews>25){
            var label = document.createElement('img');
            label.setAttribute('class', 'tag-label-detail');
            label.src = "img/Group 3030.svg";
            $(".div-tag").append(label);
        }else if(response.data.attributes.isNew==1){
            var label = document.createElement('img');
            label.setAttribute('class', 'tag-label-detail');
            label.src = "img/Group 3037.svg";
            $(".div-tag").append(label);
        }
        console.log(response.data.attributes);

        for (var i = 0; i < Math.round(response.data.attributes.rating); i++) {
            var i_star = document.createElement('i');
            i_star.setAttribute('class', 'star-rating-icon fa fa-star fa-star-filled active');
            $(".star-rating").append(i_star);
        }
        for (var j = 0; j < 5-(Math.round(response.data.attributes.rating)); j++) {
            var i_nostar = document.createElement('i');
            i_nostar.setAttribute('class', 'star-rating-icon fa fa-star');
            $(".star-rating").append(i_nostar);
        }
        // $.each(response.data, function(index, value) {
        //     console.log(value);
        // });
    });
</script>
<?php include 'script.php';?>