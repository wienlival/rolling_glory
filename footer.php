<footer class="bg-dark pt-5">
    <div class="container">
        <div class="row pb-3">
            <div class="col-md-6 mb-4">
                <a class="m-3" href="#"><i class="fa fa-instagram text-light" style="font-size: 2rem"></i></a>
                <a class="m-3" href="#"><i class="fa fa-facebook text-light" style="font-size: 2rem"></i></a>
                <a class="m-3" href="#"><i class="fa fa-twitter text-light" style="font-size: 2rem"></i></a>
            </div>
            <div class="col-md-6 mb-4">
                <div class="pb-4 text-light opacity-50 text-center text-md-start"><a class="text-light" href="#">Terms & Condition</a> | Copyright © 2018. All rights reserved. PT Your Company</div>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
