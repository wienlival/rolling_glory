<?php include 'header.php';?>
<div class="container pb-5 mb-2 mb-md-4">
  <div class="row">
    <aside class="col-lg-4">
      <div class="widget widget-filter mt-4">
        <h6>Filter</h6>
        <hr>
        <div class="card mt-3 p-3">
          <ul class="p-0 m-0">
            <li class="widget-filter-item d-flex justify-content-between align-items-center mb-1">
              <label class="form-check-label widget-filter-item-text" for="skechers">Rating 4 ke atas</label>
              <input class="form-check-input" type="checkbox" id="skechers">
            </li>
            <li class="widget-filter-item d-flex justify-content-between align-items-center mb-1">
              <label class="form-check-label widget-filter-item-text" for="skechers">Stok tersedia</label>
              <input class="form-check-input" type="checkbox" id="skechers1">
            </li>
          </ul>
        </div>
      </div>
    </aside>
    <!-- Content  -->
    <section class="col-lg-8">
      <!-- Toolbar-->
      <div class="d-flex justify-content-center justify-content-sm-between align-items-center pt-2">
        <div class="d-flex flex-wrap">
          <h6 class="mb-0">Product Category</h6>
        </div>
        <div class="d-none d-sm-flex">
          <div class="d-flex align-items-center flex-nowrap pb-3">
            <label class="opacity-75 text-nowrap fs-sm me-2 d-none d-sm-block" for="sorting">Urutkan </label>
            <select class="form-select p-1" id="sorting">
              <option>Terbaru</option>
              <option>Ulasan</option>
              <option>High - Low Price</option>
              <option>Average Rating</option>
              <option>A - Z Order</option>
              <option>Z - A Order</option>
            </select>
          </div>
        </div>
      </div>
      <hr>
      <!-- Products grid-->
      <div class="row mx-n2 mb-3" id="product_list">
      </div>
    </section>
  </div>
</div>
<?php include 'footer.php';?>
<script>
    var settings = {
        "url": "https://recruitment.dev.rollingglory.com/api/v2/gifts?page[number]=1&page[size]=6",
        "method": "GET",
        "timeout": 0,
    };
    var list = document.getElementById('product_list');

    $.ajax(settings).done(function (response) {
        $.each(response.data, function(index, value) {
            var div = document.createElement('div');
            div.setAttribute('class', 'col-md-4 col-sm-6 px-2 mt-3');
            list.appendChild(div);

            var card = document.createElement('div');
            card.setAttribute('class', 'card');
            div.appendChild(card);

            var card_title = document.createElement('span');
            card_title.setAttribute('style', 'font-weight: 500');
            if(value.attributes.stock<5){
                card_title.textContent=" Stock < 5";
                card_title.setAttribute("class","card-title px-4 py-3 mb-0 text-danger");
            }else if(value.attributes.stock=0){
                card_title.textContent=" Sold Out";
                card_title.setAttribute("class","card-title px-4 py-3 mb-0 text-danger");
            }else{
                card_title.textContent=" In Stock";
                card_title.setAttribute("class","card-title px-4 py-3 mb-0 text-success");
            }
            card.appendChild(card_title);

            var card_body = document.createElement('div');
            card_body.setAttribute('class', 'card-body py-2');
            card.appendChild(card_body);

            if(Math.round(value.attributes.rating)>=4 && value.attributes.numOfReviews>25 && value.attributes.isNew==1){
                var label = document.createElement('img');
                label.setAttribute('class', 'tag-label');
                label.src = "img/Group 3036.svg";
                card.appendChild(label);
            }else if(Math.round(value.attributes.rating)>=4 && value.attributes.numOfReviews>25){
                var label = document.createElement('img');
                label.setAttribute('class', 'tag-label');
                label.src = "img/Group 3030.svg";
                card.appendChild(label);
            }else if(value.attributes.isNew==1){
                var label = document.createElement('img');
                label.setAttribute('class', 'tag-label');
                label.src = "img/Group 3037.svg";
                card.appendChild(label);
            }

            var a_image = document.createElement('a');
            a_image.setAttribute('class', 'card-img-top d-block overflow-hidden');
            if(value.attributes.stock=0){}else{
                a_image.setAttribute('href', 'detail.php?id='+value.attributes.id);
            }

            var img = document.createElement('img');
            img.setAttribute('style', 'max-height:210px');
            img.src = value.attributes.images[0];
            card_body.appendChild(a_image);
            a_image.appendChild(img);

            var h3 = document.createElement('h3');
            h3.setAttribute('class', 'product-title fs-sm mt-3');
            h3.textContent = value.attributes.name;
            card_body.appendChild(h3);

            var div_info = document.createElement('div');
            div_info.setAttribute('class', 'd-flex justify-content-between');
            card_body.appendChild(div_info);

            var div_left = document.createElement('div');
            div_info.appendChild(div_left);

            var product_info = document.createElement('div');
            product_info.setAttribute('class', 'product-price');
            div_left.appendChild(product_info);

            var small_poin = document.createElement('small');
            small_poin.setAttribute('class', 'text-success');
            small_poin.textContent = "  " + value.attributes.points + " poins";

            var i_poin = document.createElement('i');
            i_poin.setAttribute('class', 'fa fa-star-of-david text-success');
            product_info.appendChild(i_poin);
            product_info.appendChild(small_poin);

            var div_star = document.createElement('div');
            div_star.setAttribute('class', 'star-rating');
            div_left.appendChild(div_star);

            for (var i = 0; i < Math.round(value.attributes.rating); i++) {
                var i_star = document.createElement('i');
                i_star.setAttribute('class', 'star-rating-icon fa fa-star fa-star-filled active');
                div_star.appendChild(i_star);
            }
            for (var j = 0; j < 5-(Math.round(value.attributes.rating)); j++) {
                var i_nostar = document.createElement('i');
                i_nostar.setAttribute('class', 'star-rating-icon fa fa-star');
                div_star.appendChild(i_nostar);
            }

            var small_review = document.createElement('small');
            small_review.setAttribute('class', 'opacity-50');
            small_review.textContent = "   " + value.attributes.numOfReviews + " review";
            div_star.appendChild(small_review);

            var div_right = document.createElement('div');
            div_right.setAttribute('class', 'd-flex');
            div_info.appendChild(div_right);

            var btn_wishlist = document.createElement('button');
            btn_wishlist.setAttribute('class', 'btn btn-primary btn-sm d-block w-10 mb-2');
            btn_wishlist.setAttribute('type', 'button');
            div_right.appendChild(btn_wishlist);

            var i_heart = document.createElement('i');
            i_heart.setAttribute('class', 'fa fa-heart');
            btn_wishlist.appendChild(i_heart);

            console.log(value);
        });
    });
</script>
<?php include 'script.php';?>